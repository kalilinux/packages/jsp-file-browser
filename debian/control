Source: jsp-file-browser
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>,
           Ben Wilson <g0tmi1k@kali.org>,
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.6.2
Homepage: https://www.vonloesch.de/filebrowser.html
Vcs-Git: https://gitlab.com/kalilinux/packages/jsp-file-browser.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/jsp-file-browser

Package: jsp-file-browser
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         kali-defaults,
Suggests: tomcat8 | jetty9
Description: File browser java server page
 This package contains an easy to use and easy to install file browser java
 server page. This JSP program allows remote web-based file access and
 manipulation. Features:
    - Create, copy, move, rename and delete files and directories
    - Shortkeys
    - View Files (pictures, movies, pdf, html,...)
    - Javascript filename filter
    - Edit textfiles
    - Upload files to the server (Status via Upload monitor)
    - Download files from the server
    - Download groups of files and folders as a single zip file that is created
      on the fly
    - Execute native commands on the server (e.g ls, tar, chmod,...)
    - View entries and unpack zip, jar, war and gz files on the server
    - Just one file, very easy to install (in fact, just copy it to the server)
    - Customizable layout via css file
    - Restrict file access via black or whitelist
    - Changeable to a read-only (with or without upload) solution
 Jsp file browser should work on any JSP1.1 compatible server (e.g.
 Tomcat>=3.0). It has been tested on Tomcat 4.0 and 5.5, Resin 2.1.7 and Jetty.
